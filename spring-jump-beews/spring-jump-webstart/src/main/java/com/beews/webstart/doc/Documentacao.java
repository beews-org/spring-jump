package com.beews.webstart.doc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import com.beews.webstart.properties.AppProperties;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public abstract class Documentacao {

	private String apiVersao;
	private String apiDescricao;
	private Contact contato;
	private String licensaUrl;
	private String termosServico;
	private String license;

	protected String pathsRastreados() {
		return "/api/*";
	}

	public Documentacao(AppProperties appProperties) {
		apiVersao = appProperties.getVersao();
		apiDescricao = appProperties.getDescricao();
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()).build().apiInfo(apiInfo()).useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.GET, configMenssagemDeRespostas())
				.globalResponseMessage(RequestMethod.POST, configMenssagemDeRespostas())
				.globalResponseMessage(RequestMethod.PUT, configMenssagemDeRespostas())
				.globalResponseMessage(RequestMethod.PATCH, configMenssagemDeRespostas())
				.globalResponseMessage(RequestMethod.DELETE, configMenssagemDeRespostas())

		;
	}

	private ApiInfo apiInfo() {
		return new ApiInfo(usaTitulo(), usaDescricao(), usaVersao(), usaTermosDeservico(), contato, license, licensaUrl,
				Collections.emptyList());

	}

	private String usaVersao() {
		return apiVersao;
	}

	private String usaDescricao() {
		return "Descrição dos endpoints da aplicação " + (apiDescricao != null ? apiDescricao : "");
	}

	private String usaTitulo() {
		return apiDescricao != null ? apiDescricao.toUpperCase() : apiDescricao;
	}

	private String usaTermosDeservico() {
		return termosServico != null ? "Uso exclusivo para " + termosServico : null;
	}

	private List<ResponseMessage> configMenssagemDeRespostas() {
		List<ResponseMessage> mensagensReposta = new ArrayList<>();
		mensagensReposta.add(new ResponseMessageBuilder().code(500).message("Ocorreu um erro interno")
				.responseModel(new ModelRef("Error")).build());

		mensagensReposta.add(new ResponseMessageBuilder().code(403).message("Acesso não autorizado").build());

		return mensagensReposta;

	}

}
